#include "Enemy.h"

Enemy::Enemy(int id,int wave,int type,int health,int damage){
	this->id=id;
	this->wave=wave;
	this->type=type;
	this->health=health;
	this->damage=damage;
	std::random_device rd;
    	std::mt19937 gen(rd());
    	std::uniform_int_distribution<> dis(60, 140);
	this->x=dis(gen);
	this->isAttack=false;
}
Enemy::~Enemy(){
	std::cout<<"Enemy DIED!"<<std::endl;
}
