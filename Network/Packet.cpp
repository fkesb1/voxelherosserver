#include "Packet.h"
#include "../json/json.h"

std::string Packet::packPacket(uint8_t packetType,bool secure,std::string body,std::string key){
        Json::Value root;
	Json::StyledWriter writer;
	root["packetType"]=packetType;
	root["secure"]=secure;
	root["body"]=body;
	root["key"]=key;
	std::string tmp=writer.write(root);
	return tmp;
}
