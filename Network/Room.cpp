#include <boost/asio.hpp>
#include <boost/functional/hash.hpp>
#include <iostream>
#include "Room.h"
#include "../json/json.h"
std::vector<Room*> room_list;
Room::Room(){
	boost::hash<Room*> string_hash;
	this->code=string_hash(this);
	this->isStart=false;
	this->money=0;
	this->maxHp=1500;
	this->currentHp=1500;
	this->damage=25;
	this->currentWave=0;
	this->id=0;
	room_list.push_back(this);
	std::cout<<"Room Created "<<this->code<<std::endl;
}
Room::~Room(){
	for(int i=0;i<this->EnemyList.size();i++){
		delete this->EnemyList[i];
		EnemyList[i]=NULL;
	}
	for(int i=0;i<this->playerList.size();i++){
		delete this->playerList[i];
		playerList[i]=NULL;
	}
	std::cout<<"Room destroyed"<<std::endl;
}
void Room::createRoom(Room *r){
	
}
int Room::joinRoom(User *u){
	if(this->playerList.size()<=3){
		this->playerList.push_back(u);
		return 0;
	}
	else return -1;
}
void Room::gameStartRequest(){
	if(this->isStart==true){
		std::cout<<"Game Alread started code: "<<this->code<<std::endl;
		return;
	}
	this->isStart=true;
	this->currentWave=1;
	std::cout<<"GameStart "<<this->code<<std::endl;
	this->spawnEnemy();
}
int Room::findRoom(std::string username){
	User *u=User::findUser(username);
	if(u==nullptr){
		return -1;
	}
	for(int i=0;i<room_list.size();i++){
		if(room_list[i]->isStart==false && room_list[i]->playerList.size()<=3){
			room_list[i]->joinRoom(u);
			return room_list[i]->code;
		}
	}
	Room *r=new Room();
	r->joinRoom(u);
	return r->code;
}
Room* Room::getRoomInfo(int code){
	for(int i=0;i<room_list.size();i++){
                if(room_list[i]->code==code){
                        return room_list[i];
                }
        }
	return NULL;
}
std::vector<User*> Room::getPlayerList(){
	return this->playerList;
}

int Room::killRoom(boost::asio::ip::udp::endpoint e,std::string localIp){
	return -1;
}
void Room::updateInGameStatus(int money,int maxHp,int currentHp,int currentWave,int damage){
	this->money=money;
	this->maxHp=maxHp;
	this->currentHp=currentHp;
	this->currentWave=currentWave;
	this->damage=damage;
}
std::string Room::getInGameStatus(){
	Json::Value root;
	Json::StyledWriter write;
	root["money"]=this->money;
	root["maxHp"]=this->maxHp;
	root["currentHp"]=this->currentHp;
	root["currentWave"]=this->currentWave;
	root["damage"]=this->damage;
	return write.write(root);
}
void Room::upgradeCastleHp(){
	if(this->money>=0){
		std::cout<<"CastleHpUpgrade code: "<<this->code<<std::endl;
		this->maxHp+=20;
		this->money-=0;
	}
}
void Room::repaireCastleHp(){
	if(this->money>=0){
		this->currentHp=this->maxHp;
		this->money-=0;
	}
}
void Room::upgradeDamage(){
	if(this->money>=0){
		this->damage+=5;
		this->money-=0;
	}
}
void Room::spawnEnemy(){
	int health=this->currentWave*15+100;
	int tmp=this->id;
	for(int i=tmp;i<tmp+7;i++){
		Enemy *e=new Enemy(this->id,this->currentWave,1,health,10);
		this->EnemyList.push_back(e);
		this->id++;
	}
	tmp=this->id;
	/*for(int i=tmp;i<tmp+3;i++){
		Enemy *e=new Enemy(this->id,this->currentWave,2,health,10);
		this->EnemyList.push_back(e);
		this->id++;
	}*/
	tmp=this->id;
	for(int i=tmp;i<tmp+5;i++){
		Enemy *e=new Enemy(this->id,this->currentWave,3,health,10);
		this->EnemyList.push_back(e);
		this->id++;
	}
}
std::string Room::getEnemyList(){
	Json::Value root;
	Json::Value root2;
	Json::StyledWriter write;
	int tmp=0;
	for(int i=0;i<this->EnemyList.size();i++){
		if(this->EnemyList[i]->wave!=this->currentWave) continue;
		root2["id"]=this->EnemyList[i]->id;
		root2["type"]=this->EnemyList[i]->type;
		root2["x"]=this->EnemyList[i]->x;
		root[tmp]=root2;
		tmp++;
	}
	return write.write(root);
}
int Room::damageHp(int dmg){
	int playerCount=this->playerList.size();
	if(this->currentHp<=0){
		return 0;
	}
	else{
		this->currentHp-=dmg/playerCount;
		return this->currentHp;
	}
}
int Room::damageEnemy(int id){
	Enemy *e;
	int i;
	bool flag=true;
	std::cout<<id<<std::endl;
	for(i=0;i<this->EnemyList.size();i++){
		if(id==this->EnemyList[i]->id){
			flag=false;
			e=EnemyList[i];
			break;
		}
	}
	if(flag) return 0;
	e->health=e->health-this->damage;
	if(e->health<=0){
		this->EnemyList.erase(this->EnemyList.begin()+i);
		delete e;
		e=NULL;
		this->money+=50;
		return 0;
	}
	else return e->health;
}
std::string Room::waveEnd(int wave){
	Json::Value root;
	Json::StyledWriter writer;
	if(wave!=this->currentWave){
		root["wave"]=this->currentWave;
		root["code"]=this->natureCode;
		root["duration"]=this->natureDuration;
		root["amount"]=this->amount;
		return writer.write(root);
	}
	std::cout<<"Wave End"<<std::endl;
	this->currentWave++;
	if(this->currentWave>=50){
		std::cout<<"GameClear"<<std::endl;
		return "";
	}
	else{
		this->natureCode=this->GetnatureCode();
		this->natureDuration=this->GetnatureDuration();
		this->amount=this->Getamount();
		root["wave"]=this->currentWave;
		root["code"]=this->natureCode;
		root["duration"]=this->natureDuration;
		root["amount"]=this->amount;
		this->spawnEnemy();
		return writer.write(root);
	}
}
int Room::GetnatureCode(){
	if(this->currentWave<=2) return 0;
	else if(this->currentWave==3) return 1;
	else if(this->currentWave>=48) return 1;
	else{
		std::random_device rd;
		std::mt19937 gen(rd());
        	std::uniform_int_distribution<> dis(0, 6);
        	if(dis(gen)==5) return 1;
		else return 0;
	}	
}
int Room::GetnatureDuration(){
	if(this->currentWave<=10) return 10;
	else if(this->currentWave<=20) return 15;
	else return 20;
}
float Room::Getamount(){
	if(this->currentWave<=3) return 0.18;
	else if(this->currentWave>=48) return 0.5;
	else{
		std::random_device rd;
		std::mt19937 gen(rd());
        	std::uniform_int_distribution<> dis(1, 5);
        	return 0.1*dis(gen);
	}
}
int Room::orbitalStrike(){
	if(this->money>=0){
		this->money-=0;
		for(int i=0;i<this->EnemyList.size();i++){
			delete this->EnemyList[i];
			this->EnemyList[i]=NULL;
		}
		return this->money;
	}
	return 0;
}
