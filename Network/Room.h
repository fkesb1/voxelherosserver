#include <boost/asio.hpp>
#include "../User/User.h"
#include "Enemy.h"
#include "Handle.h"
#include <list>
class Room{
	public:
		int code;
		std::vector<User*> playerList;
		Room();
		~Room();
		void createRoom(Room*);
		int joinRoom(User*);
		void gameStartRequest();
		void inGameHandle();
		std::vector<User*> getPlayerList();
		static int findRoom(std::string);
		static Room* getRoomInfo(int);
		void updateInGameStatus(int,int,int,int,int);
		std::string getInGameStatus();
		static int killRoom(boost::asio::ip::udp::endpoint,std::string);
		bool isStart;
		void upgradeCastleHp();
		void repaireCastleHp();
		void upgradeDamage();
		std::string getEnemyList();
		int damageHp(int);
		int damageEnemy(int);
		std::string waveEnd(int);
		int orbitalStrike();
	private:
		int id;
		int money;
		int maxHp;
		int currentHp;
		int damage;
		int currentWave;
		std::vector<Enemy*> EnemyList;
		void spawnEnemy();
		void damageHp();
		int natureCode;
		int natureDuration;
		float amount;
		int GetnatureCode();
		int GetnatureDuration();
		float Getamount();
};
