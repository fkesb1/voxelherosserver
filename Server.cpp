#include "Server.h"

using boost::asio::ip::udp;

Server::Server(boost::asio::io_service& io):serverSocket(io,udp::endpoint(udp::v4(),51000)){
	this->serverName="IFCFG";
	this->serverType="BETA";
	startRecv();
}
void Server::startRecv(){
	serverSocket.async_receive_from(boost::asio::buffer(recvBuf),remote_endpoint,[this](boost::system::error_code errCode,std::size_t bytesReceived){
		if(!errCode && bytesReceived>0){
			std::cout<<"DataReceived "<<bytesReceived<<" bytes"<<std::endl;
			//std::vector<uint8_t> data(recvBuf.begin(),recvBuf.begin()+bytesReceived);
			recvQue.push_back(recvBuf;);
			//parseData(data,remote_endpoint);
		}
		else{
			std::cout<<"ERROR"<<std::endl;
		}
		startRecv();
	});
}
void Server::parseData(std::vector<uint8_t> data,udp::endpoint e){
	Json::Reader reader;
	int type=-1;
	bool secure=false;
	std::string body="";
	std::string session="";
	if(parsePacket(&type,&secure,&body,&session,data)!=0){
		std::cout<<"ERROR Parse Packet"<<std::endl;
		return;
	}
	switch(type){
		case PacketType::OP_login:{
			std::string username=root["body"].asString();
			int privilege=1;
			/*Login::ParseData(data,&username,&privilege);
			std::string username="";
			int privilege=-1;
			Login::ParseData(body,&username,&privilege);
			if(username=="" || privilege==-1){
				std::cout<<"LOGIN FAIL "<<std::endl;
				sendData(e,Packet::packPacket(PacketType::OP_loginReply,false,Login::packData(-1,privilege),""));
				return;
			}
			std::cout<<"LOGIN SUCCESS"<<std::endl;*/
			User *u=new User(username,privilege,e);
			sendData(e,Packet::packPacket(PacketType::OP_loginReply,false,Login::packData(0,privilege),""));
		} break;
		case PacketType::OP_ServerStatus:{
			Json::Value root2;
			Json::StyledWriter writer;
			root2["serverName"]=this->serverName;
			root2["mode"]=this->serverType;
			std::string data=writer.write(root2);
			sendData(e,Packet::packPacket(PacketType::OP_ServerStatusReply,false,data,""));
		} break;
		case PacketType::OP_CreateAccount:{
			if(!Login::Register(body)){
				sendData(e,Packet::packPacket(PacketType::OP_CreateAccountReply,false,"-1",""));
			}
			else{
				sendData(e,Packet::packPacket(PacketType::OP_CreateAccountReply,false,"0",""));
			}
		} break;
		case PacketType::OP_CreateRoom: {
			Room *r=new Room();
			std::string username=body;
			User *u=User::findUser(username);
			if(u==nullptr){
				delete r;
				r=NULL;
				sendData(e,Packet::packPacket(PacketType::OP_CreateRoomReply,false,"",""));
			}
			else{
				std::cout<<u->username<<std::endl;
				r->joinRoom(u);
				sendData(e,Packet::packPacket(PacketType::OP_CreateRoomReply,false,"planetside",""));
			}
		} break;
		case PacketType::OP_FindRoom:{
			std::string username=body;
			int code=Room::findRoom(username);
			if(code==-1){
				sendData(e,Packet::packPacket(PacketType::OP_CreateRoomReply,false,std::to_string(-1),""));
			}
			else{
				sendData(e,Packet::packPacket(PacketType::OP_FindRoomReply,false,std::to_string(code),""));
			}
		} break;
		case PacketType::OP_gameStartRequest:{
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			else{
				r->gameStartRequest();
				sendData(r->playerList,Packet::packPacket(PacketType::OP_GameStart,false,"",""));
			}
		} break;
		case PacketType::OP_GameStatus:{
			Json::Value root2;
			bool isSuccess2=reader.parse(body,root2);
			if(!isSuccess2){
				std::cout<<"ERROR PARSE DATA"<<std::endl;
				return;
			}
			int code=root2["code"].asInt();
			int money=root2["money"].asInt();
			int maxHp=root2["maxHp"].asInt();
			int currentHp=root2["currentHp"].asInt();
			int currentWave=root2["currentWave"].asInt();
			int damage=root2["damage"].asInt();
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			r->updateInGameStatus(money,maxHp,currentHp,currentWave,damage);
			sendData(r->getPlayerList(),Packet::packPacket(PacketType::OP_GameStatusReply,false,body,""));
		} break;
		case PacketType::OP_GameStatusRequest:{
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			std::string data=r->getInGameStatus();
			sendData(r->playerList,Packet::packPacket(PacketType::OP_GameStatusReply,false,data,""));
		} break;
		case PacketType::OP_UpgradeHp:{
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			r->upgradeCastleHp();
			std::string data=r->getInGameStatus();
			sendData(r->playerList,Packet::packPacket(PacketType::OP_GameStatusReply,false,data,""));
		} break;
		case PacketType::OP_RepairHp : {
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			r->repaireCastleHp();
			std::string data=r->getInGameStatus();
			sendData(r->playerList,Packet::packPacket(PacketType::OP_GameStatusReply,false,data,""));	
		} break;
		case PacketType::OP_DamageUp:{
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			r->upgradeDamage();
			std::string data=r->getInGameStatus();
			sendData(e,Packet::packPacket(PacketType::OP_GameStatusReply,false,data,""));	
		} break;
		case PacketType::OP_EnemyListRequest: {
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			std::string data=r->getEnemyList();
			sendData(e,Packet::packPacket(PacketType::OP_EnemyListReply,false,data,""));	
		} break;
		case PacketType::OP_PlayerMove:{
			std::string room_code=session;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			sendData(r->playerList,e,Packet::packPacket(PacketType::OP_PlayerMove,false,body,""));
		} break;
		case PacketType::OP_CastleDamage:{
			std::string room_code=session;
			int code=std::stoi(room_code);
			std::string e_damage=body;
			int damage=std::stoi(e_damage);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			int hp=r->damageHp(damage);
			std::string data=std::to_string(hp);
			if(hp<=0){
				sendData(r->playerList,Packet::packPacket(PacketType::OP_GameOver,false,"",""));
				delete r;
				r=NULL;
			}
			else{
				sendData(r->playerList,Packet::packPacket(PacketType::OP_CastleDamage,false,data,""));
			}
		} break;
		case PacketType::OP_EnemyHit:{
			std::string room_code=session;
			int code=std::stoi(room_code);
			Json::Value root2;
			Json::StyledWriter writer;
			std::cout<<body<<std::endl;
			bool isSuccess2=reader.parse(body,root2);
			if(!isSuccess2){
				std::cout<<"ERROR PARSE DATA"<<std::endl;
				return;
			}
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			int hp=r->damageEnemy(root2["id"].asInt());
			root2["hp"]=hp;
			body=writer.write(root2);
			if(hp>0){
				sendData(r->playerList,Packet::packPacket(PacketType::OP_EnemyHit,false,body,""));
			}
			else{
				sendData(r->playerList,Packet::packPacket(PacketType::OP_Enemydie,false,body,""));
			}
		} break;
		case PacketType::OP_WaveEnd:{
			std::string room_code=session;
			int code=std::stoi(room_code);
			std::string e_wave=body;
			int wave=std::stoi(e_wave);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			std::string re=r->waveEnd(wave);
			if(re.length()==0){
				sendData(r->playerList,Packet::packPacket(PacketType::OP_GameClear,false,"",""));
				delete r;
				r=NULL;
			}
			else{
				sendData(r->playerList,Packet::packPacket(PacketType::OP_WaveEnd,false,re,""));
			}
		} break;
		case PacketType::OP_OrbitalStrikeRequest:{
			std::string room_code=body;
			int code=std::stoi(room_code);
			Room *r=Room::getRoomInfo(code);
			if(r==nullptr){
				std::cout<<"ERROR"<<std::endl;
				return;
			}
			int re=r->orbitalStrike();
			if(re>0){
				sendData(r->playerList,Packet::packPacket(PacketType::OP_OrbitalStrikeReply,false,std::to_string(re),""));	
			}
		} break;
		default:{
			std::cout<<"WRONG PACKET"<<std::endl;
		} break;
	}
}
int Server::parsePacket(int* type,bool* secure,std::string *body,std::string *session,std::vector<uint8_t> data){
	Json::Reader reader;
	Json::Value root;
	std::string str(data.begin(),data.end());
	bool isSuccess=reader.parse(str,root);
	if(!isSuccess){
		std::cout<<"ERROR PARSE DATA"<<std::endl;
		return -1;
	}
	try{
		*type=root["type"].asInt();
		*secure=root["secure"].asBool();
		*body=root["body"].asString();
		*session=root["session"].asString();
		return 0;
	}
	catch(std::exception& e){
		std::cout<<"PARSE Exception "<<std::endl;
		return -1;
	}
}
void Server::sendData(boost::asio::ip::udp::endpoint e,std::string data){
	serverSocket.async_send_to(boost::asio::buffer(data),e,[this](boost::system::error_code errCode,std::size_t bytesTransfered){
		if(!errCode && bytesTransfered>0){
			std::cout<<"Data transfered "<<bytesTransfered<<" bytes"<<std::endl;
		}
		else{
			std::cout<<"ERROR "<<errCode.message()<<std::endl;
		}
	});
}
void Server::sendData(std::vector<User*> e,std::string data){
	for(int i=0;i<e.size();i++){
		serverSocket.async_send_to(boost::asio::buffer(data),e[i]->e,[this](boost::system::error_code errCode,std::size_t bytesTransfered){
                	if(!errCode && bytesTransfered>0){
                        	std::cout<<"[Multicast]Data transfered "<<bytesTransfered<<" bytes"<<std::endl;
                	}
                	else{
                        	std::cout<<"ERROR "<<errCode.message()<<std::endl;
                	}
        	});
	}
}
void Server::sendData(std::vector<User*> e,boost::asio::ip::udp::endpoint mye,std::string data){
	for(int i=0;i<e.size();i++){
		if(e[i]->e==mye) continue;
		serverSocket.async_send_to(boost::asio::buffer(data),e[i]->e,[this](boost::system::error_code errCode,std::size_t bytesTransfered){
                	if(!errCode && bytesTransfered>0){
                        	std::cout<<"[Multicast]Data transfered "<<bytesTransfered<<" bytes"<<std::endl;
                	}
                	else{
                        	std::cout<<"ERROR "<<errCode.message()<<std::endl;
                	}
        	});
	}
}
Server::~Server(){

}
