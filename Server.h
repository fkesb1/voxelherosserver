#include <iostream>
#include <boost/asio.hpp>
#include <array>
#include "json/json.h"
#include "Network/Packet.h"
#include "Network/op_code.h"
#include "Network/Room.h"
#include "User/Login.h"
#include "User/User.h"
#include "database/mysql.h"

using boost::asio::ip::udp;
class Server{
	public:
		Server(boost::asio::io_service&);
		~Server();
	private:
		void startRecv();
		void sendData(boost::asio::ip::udp::endpoint,std::string);
		void sendData(std::vector<User*>,std::string);
		void sendData(std::vector<User*>,boost::asio::ip::udp::endpoint,std::string);
		void parseData(std::vector<uint8_t>,udp::endpoint);
		int parsePacket(int*,bool*,std::string*,std::string*,std::vector<uint8_t>);
		udp::socket serverSocket;
		std::string serverName;
		std::string serverType;
		std::array<uint8_t,1024> recvBuf;
		udp::endpoint remote_endpoint;
		std::queue<array> recvQue;
};
