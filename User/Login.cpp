#include "Login.h"

void Login::ParseData(std::string data,std::string* userid,int* privilege){
    Json::Value root;
    Json::Reader reader;
    if(!reader.parse(data,root)){
           return;
    }
    int privilege_num=-1;
    std::string username=root["username"].asString();
    std::string password=root["password"].asString();
    float version=root["version"].asFloat();
    std::cout<<"USER TRYING LOGIN "<<" USERNAME: "<<username<<" PASSWORD: "<<password<<std::endl;
    Login::check(username,password,&privilege_num);
    *userid=username;
    *privilege=privilege_num;
}
void Login::check(std::string username,std::string password,int* privilege){
    mysql *m=new mysql("localhost","planetside","ps2115739","game");
    std::string query="SELECT username,privilege FROM user WHERE username=? AND password=?";
    m->stmt = m->con->prepareStatement(query);
    m->stmt->setString(1,username);
    m->stmt->setString(2,password);
    m->res=m->stmt->executeQuery();
    if(m->res!=NULL && m->res->rowsCount()==1){
        m->res->next();
        *privilege=std::stoi(m->res->getString("privilege"));
    }
    delete m->res;
    delete m->stmt;
    delete m;
}
int Login::Register(std::string data){
    Json::Value root;
    Json::Reader reader;
    mysql *m=new mysql("localhost","planetside","ps2115739","game");
    std::string query="INSERT INTO user(username,password) VALUES(?,?)";
    bool isSuccess=reader.parse(data,root);
	if(!isSuccess){
		std::cout<<"ERROR PARSE DATA"<<std::endl;
		return 1;
	}
    std::string username=root["username"].asString();
    std::string password=root["password"].asString();
    if(username.length()<=5 || password.length()<=5){
        return 1;
    }
    m->stmt = m->con->prepareStatement(query);
    m->stmt->setString(1,username);
    m->stmt->setString(2,password);
    try{
        bool re=m->stmt->execute();       
        if(!re){
            return 0;
        }
        else return 1;
    }
    catch(sql::SQLException){
        return 1;
    }
}
std::string Login::packData(int code,int privilege){
    Json::Value root;
    Json::StyledWriter writer;
    root["code"]=code;
    root["privilege"]=privilege;
    return writer.write(root);
}