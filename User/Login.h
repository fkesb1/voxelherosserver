#include <iostream>
#include "../json/json.h"
#include "../database/mysql.h"
#include "../json/json.h"
class Login{
    public:
        static void ParseData(std::string,std::string*,int*);
        static int Register(std::string);
        static std::string packData(int,int);
    private:
        static void check(std::string,std::string,int*);
};