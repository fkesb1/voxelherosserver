#include "User.h"
std::vector<User*> User::userList;
User::User(std::string username,int privilege,boost::asio::ip::udp::endpoint e){
	this->username=username;
	this->e=e;
    this->privilege=privilege;
	User::userList.push_back(this);
}
User* User::findUser(std::string username){
	for(int i=0;i<userList.size();i++){
		if(!userList[i]->username.compare(username)){
			return userList[i];
		}
	}
	return NULL;
}
User::~User(){
	for(int i=0;i<User::userList.size();i++){
		if(userList[i]==this){
			userList.erase(User::userList.begin()+i);
		}
	}
}
