#include <boost/asio.hpp>
#ifndef USER_H
#define USER_H
class User{
    public:
		User(std::string,int,boost::asio::ip::udp::endpoint);
		~User();
		std::string username;
        int privilege;
		boost::asio::ip::udp::endpoint e;
		static std::vector<User*> userList;
		static User* findUser(std::string);
};
#endif