#include "mysql.h"
mysql::mysql(std::string host,std::string username,std::string password,std::string database){
    this->driver=get_driver_instance();
    this->con=this->driver->connect(host,username,password);
    this->con->setSchema(database);
}

mysql::~mysql(){
    delete con;
}