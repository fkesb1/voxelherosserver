#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#ifndef mysql_H
#define mysql_H
class mysql{
    public:
        mysql(std::string,std::string,std::string,std::string);
        ~mysql();
        sql::Driver *driver;
        sql::Connection *con;
        sql::PreparedStatement *stmt;
        sql::ResultSet *res;
};
#endif